/* --------------------------------------------------------------------
EXTREME TUXRACER

Copyright (C) 1999-2001 Jasmin F. Patry (Tuxracer)
Copyright (C) 2010 Extreme Tuxracer Team

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
---------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#include <etr_config.h>
#endif

#include "newplayer.h"
#include "particles.h"
#include "audio.h"
#include "gui.h"
#include "ogl.h"
#include "textures.h"
#include "font.h"
#include "game_ctrl.h"
#include "translation.h"
#include "regist.h"
#include "winsys.h"
#include "spx.h"
#include <cctype>

CNewPlayer NewPlayer;

static TUpDown* avatar;
static TWidget* textbuttons[2];
static TTextField* keybuttons[38];
static TTextField* textfield;
static bool caps = false;

void QuitAndAddPlayer () {
	if (textfield->Text().size () > 0) {
		g_game.start_player = Players.numPlayers();
		g_game.start_character = 0;
		Players.AddPlayer (textfield->Text(), Players.GetDirectAvatarName(avatar->GetValue()));
	}
	State::manager.RequestEnterState (Regist);
}

void CNewPlayer::Keyb_spec (SDL_Keysym sym, bool release) {
	if (release) return;

	KeyGUI(sym.sym, sym.mod, release);
	switch (sym.sym) {
		case SDLK_ESCAPE:
			State::manager.RequestEnterState (Regist);
			break;
		case SDLK_RETURN:
			if (textbuttons[0]->focussed()) State::manager.RequestEnterState (Regist);
			else QuitAndAddPlayer ();
			break;
		default:
			break;
	}
}

void CNewPlayer::Mouse (int button, int state, int x, int y) {
	TWidget* clicked = ClickGUI(state, x, y);
	if (state == 0) {
		if (clicked == textbuttons[0]) State::manager.RequestEnterState (Regist);
		else if (clicked == textbuttons[1]) QuitAndAddPlayer();
		else if (clicked == keybuttons[36]) {
			caps = !caps;
			int off = caps ? 55 : 87;
			for (int i = 10; i < 36; ++i) {
				char ch[2] = { char(i + off), 0 };
				keybuttons[i]->SetText(ch);
			}
		} else if (clicked == keybuttons[37]) {
			string tmp = textfield->Text();
			if (tmp.length()) {
				tmp.resize(tmp.length() - 1);
				textfield->SetText(tmp);
			}
		} else for (int i = 0; i < 36; ++i) if (clicked == keybuttons[i])
			textfield->SetText(textfield->Text() + keybuttons[i]->Text());

		if (clicked) clicked->focus = false;
	}
	textfield->focus = true;
}

void CNewPlayer::Motion (int x, int y) {
	MouseMoveGUI(x, y);

	if (param.ui_snow) push_ui_snow (cursor_pos);
}

static int prevleft, prevtop, prevwidth;

void CNewPlayer::Enter() {
	Winsys.KeyRepeat (true);
	Winsys.ShowCursor (!param.ice_cursor);
	Music.Play (param.menu_music, -1);

	int framewidth = 280 * Winsys.scale;
	int frameheight = 50 * Winsys.scale;
	//int frametop = AutoYPosN (38);
	TArea area = AutoAreaN(20, 80, framewidth + 120);
	//int prevoffs = 80;
	prevleft = area.left - (area.right-area.left)/3.5;
	prevtop = AutoYPosN(Winsys.resolution.width < Winsys.resolution.height ? 30 : 8);
	prevwidth = 128 * Winsys.scale;

	ResetGUI();

	int ptop = Winsys.resolution.width < Winsys.resolution.height ? 35 : 15;
	avatar = AddUpDown (area.left + framewidth + 88, AutoYPosN(ptop), 0, (int)Players.numAvatars() - 1, 0);

	int siz = FT.AutoSizeN (5);
	int ptop3 = Winsys.resolution.width < Winsys.resolution.height ? 80 : 85;
	textbuttons[0] = AddTextButton (Trans.Text(8), area.left+50, AutoYPosN (ptop3), siz);
	ETR_DOUBLE len = FT.GetTextWidth (Trans.Text(15));
	textbuttons[1] = AddTextButton (Trans.Text(15), area.right-len-50, AutoYPosN (ptop3), siz);

	int ptop2 = Winsys.resolution.width < Winsys.resolution.height ? 35 : 15;
	textfield = AddTextField(emptyString, area.left + 80, AutoYPosN(ptop2), framewidth, frameheight);
	textfield->focus = true;

	int spacing = Winsys.resolution.width < Winsys.resolution.height ? Winsys.resolution.width / 12 : Winsys.resolution.width / 16.5;

	int ktop = AutoYPosN(Winsys.resolution.width < Winsys.resolution.height ? 50 : 30);
	int kleft = (Winsys.resolution.width - spacing*10) / 2;
	string chars = "0123456789abcdefghijklmnopqrstuvwxyz";

	string chr;
	int x = 0;
	int y = 0;
	for (int c = 0; c < chars.length(); c++) {
		if (c % 10 == 0) {
			x = 0;
			y += 1;
		}
		chr = chars[c];
		keybuttons[c] = AddTextField(chr, kleft+spacing*x, ktop+spacing*y, spacing, spacing);
		x += 1;
	}
	keybuttons[36] = AddTextField("caps", kleft+spacing*(x+1), ktop+spacing*y, spacing*2, spacing);
	keybuttons[37] = AddTextField("<", kleft+spacing*(x+3), ktop+spacing*y, spacing, spacing);
}

void CNewPlayer::Loop() {
	int ww = Winsys.resolution.width;
	int hh = Winsys.resolution.height;
	TColor col;

	Music.Update ();
	check_gl_error();
	ClearRenderContext ();
	ScopedRenderMode rm(GUI);
	SetupGuiDisplay ();

	if (param.ui_snow) {
		update_ui_snow ();
		draw_ui_snow();
	}

	textfield->UpdateCursor();

//	DrawFrameX (area.left, area.top, area.right-area.left, area.bottom - area.top,
//			0, colMBackgr, col, 0.2);

	Tex.Draw (BOTTOM_LEFT, 0, hh - 256, 1);
	Tex.Draw (BOTTOM_RIGHT, ww-256, hh-256, 1);
	Tex.Draw (TOP_LEFT, 0, 0, 1);
	Tex.Draw (TOP_RIGHT, ww-256, 0, 1);
	//Tex.Draw (T_TITLE_SMALL, CENTER, AutoYPosN (5), Winsys.scale);

	FT.SetColor (colWhite);
	FT.AutoSizeN (3);
	FT.DrawString (CENTER, AutoYPosN (Winsys.resolution.width < Winsys.resolution.height ? 45 : 33), Trans.Text(66));

	if (avatar->focussed()) col = colDYell;
	else col = colWhite;
	Players.GetAvatarTexture(avatar->GetValue())->DrawFrame(
	    prevleft, prevtop, prevwidth, prevwidth, 2, col);

	textfield->focus = true;
	DrawGUI();
	Winsys.SwapBuffers();
}
